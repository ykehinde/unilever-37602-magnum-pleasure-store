/**
 * Author: Yemi Kehinde
 * Magnum Pleasure Store
 */

// Create a closure to maintain scope of the '$' and KO
;(function (KO, $) {

	$(function() {

		$('.hero-icecream').delay(500).fadeIn(900);
		$('.header--copy').delay(1500).fadeIn(900);

		KO.Config.init();
		KO.smoothScroll.init();
		KO.menu.init();
		KO.NastyBrowserSniffing.init();
		KO.NastyBrowserSniffingAndroid.init();
		KO.ScrollMagic.init();

	}); // END DOC READY


	KO.Config = {
		variableX : '', // please don't keep me - only for example syntax!

		init : function () {
			console.debug('Kickoff is running');
		}
	};

	KO.menu = {
		init : function(){
			$(document).ready(function() {
			  $('body').addClass('js');
			  var $menu = $('#menu'),
			  $menubutton = $('.menu-link');
			  $menulink = $('#menu li')
				  
			  
				$menubutton.click(function() {
				  $menubutton.toggleClass('active');
				  $menu.toggleClass('active');
				  return false;
				});

				$menulink.click(function(){
					$menubutton.removeClass('active');
					$menu.removeClass('active');
				});

			});
		}
	};

	KO.smoothScroll = {
		init : function (){
			smoothScroll.init({
				speed: 1000,
				easing: 'easeInOutQuint',
				offset: 0,
				updateURL: true,
				callbackBefore: function ( toggle, anchor ) {},
				callbackAfter: function ( toggle, anchor ) {}
			});
		}
	};

	KO.NastyBrowserSniffing = {

		ua: navigator.userAgent.toLowerCase(),

		init: function() {
		
			var isWebkit = KO.NastyBrowserSniffing.ua.indexOf("webkit") > -1;
			if (isWebkit) {
			  $("html").addClass("webkit");
			}
		}

	};

	KO.NastyBrowserSniffingAndroid = {

		ua: navigator.userAgent.toLowerCase(),

		init: function() {
		
			var isAndroid = KO.NastyBrowserSniffingAndroid.ua.indexOf("android") > -1;
			if (isAndroid) {
				$("html").removeClass("webkit");
				$("html").addClass("android");
			}
		}

	};

	KO.ScrollMagic = {
		init: function(){
			// Init ScrollMagic Controller

			var controller1 = new ScrollMagic.Controller({container: "#container1"});

			// build tween
		    var animate6 = new TimelineMax()
		        .add([
		            TweenMax.to(".step.coat", 0.5, {
		            autoAlpha: 1,
		            y: 0
		            }),
		            TweenMax.to(".step.top", 0.5, {
		            autoAlpha: 1,
		            delay:0.5,
		            y: 0
		            }),
		            TweenMax.to(".step.drizzle", 0.5, {
		            autoAlpha: 1,
		            delay:1,
		            y: 0
		            }),
		            TweenMax.to(".step.set", 0.5, {
		            autoAlpha: 1,
		            delay:1.5,
		            y: 0
		            }),
		            TweenMax.to(".step.share", 0.5, {
		            autoAlpha: 1,
		            delay:2,
		            y: 0
		            }),
		            TweenMax.to(".step.love", 0.5, {
		            autoAlpha: 1,
		            delay:2.5,
		            y: 0
		            })
		    ]);


			// build scene
			var scene = new ScrollMagic.Scene({
				triggerElement: "#yourMagnum",
	            reverse: false
			})
				.setTween(animate6)
				.addTo(controller1)
				// .addIndicators(); // add indicators (requires plugin)
				// .setPin("#container1 .animated");




			// // 1,2,3 section
			// // build tween
		 //    var animate1 = new TimelineMax()
		 //        .add([
		 //            TweenMax.to(".media--one", 0.5, {
		 //            autoAlpha: 1,
		 //            y: 0
		 //            }),
		 //            TweenMax.to(".media--two", 0.5, {
		 //            autoAlpha: 1,
		 //            delay:0.5,
		 //            y: 0
		 //            }),
		 //            TweenMax.to(".media--three", 0.5, {
		 //            autoAlpha: 1,
		 //            delay:1,
		 //            y: 0
		 //            }),
		 //    ]);

		 //    // build scene
			// var scene = new ScrollMagic.Scene({
			// 	triggerElement: "#competition",
	  //           reverse: false
			// })
			// 	.setTween(animate1)
			// 	.addTo(controller1)
			// 	// .addIndicators(); // add indicators (requires plugin)
			// 	// .setPin("#container1 .animated");
		}
	};


	// Example module
	/*
	KO.MyExampleModule = {
		init : function () {
			KO.MyExampleModule.setupEvents();
		},

		setupEvents : function () {
			//do some more stuff in here
		}
	};
	*/

})(window.KO = window.KO || {}, jQuery);
